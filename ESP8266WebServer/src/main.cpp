#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

String data = "";
bool dataReceived = false;

// Object for storin multiple APs' credentials
ESP8266WiFiMulti wifiMulti;

// Status of WiFi connection
bool connected = false;

// Object for server, more info at
// https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WebServer
ESP8266WebServer server(80);

void handleConnection();
void firstConnection();
void connectionLoop();
void handleActuate();
void handleBody(String data);
int postRequest(String URL, String data, String contentType = "application/json");

void setup() {

  // Begin Serial Communication
  Serial.begin(115200);

  // WiFi set as station aka client
  WiFi.mode(WIFI_STA);

  // Add your known networks
  wifiMulti.addAP("Q's AP, full of IOT", "natalitsa");
  wifiMulti.addAP("ssid_from_AP_2", "your_password_for_AP_2");
  wifiMulti.addAP("ssid_from_AP_3", "your_password_for_AP_3");

  // Attach routes to functions,
  // you know, like evey other freaking backend framework
  server.on("/actuate", handleActuate);

  // Start the server, obviously
  server.begin();

}

void loop() {

  handleConnection();

} // end of loop() function

void handleConnection(){
  // if the WiFi is connected
  if (wifiMulti.run() == WL_CONNECTED) {
    // And was not connected previously
    if (!connected) {

      connected = true; // update its status

      //---- Code Executed on First Connection ----
      firstConnection();

    }
    //---- LOOP while connected ----
    connectionLoop();

    //---- END of loop ----

  // WiFi not connected
  }else{
    connected = false; // update its status
    if (!connected)
      Serial.println("WiFi Disconnected!");
    delay(1000);
  }

}

void firstConnection(){

  Serial.println("Connection established!"); // notify the user
  Serial.print("IP address:\t"); // print the new IP address
  Serial.println(WiFi.localIP());

  String url = "http://192.168.1.3:3000/actuatorDeployed";
  String json = "{\"location\":\"livingroom\",\"actuator\":\"lights\",\"range\":{\"min\":\"off\",\"max\":\"on\"}}";

  // Retry until the POST request is successful and the microcontroller known to the network
  int code = -1;
  while(code < 0){
    code = postRequest(url, json);
    delay(500);
  }
}

void connectionLoop(){
  // Handling of incoming requests
  server.handleClient();
}

// Handle the incoming request on /actuate route
void handleActuate() {

  // clear data String to populate with the new request body
  data = "";

  // Check if body received
  if (server.hasArg("plain")== false){
    server.send(200, "text/plain", "Body not received");
    return;
  }

  //
  Serial.println("YAY! New Request!");
  dataReceived = true;
  String message = "Body received:\n";
         data += server.arg("plain"); // Request Body
         message += data;
         message += "\n";
  server.send(200, "text/plain", message); // Response

  handleBody(data);
}

// Handles the request body
// Parse the Json input and
// Actuate the actuators
void handleBody(String data){

  // Allocate a temporary JsonDocument
  StaticJsonDocument<1024> doc;

  // Deserialize the JSON string
  DeserializationError error = deserializeJson(doc, data);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  String location = doc["location"];
  String actuator = doc["actuator"];
  String value = doc["value"];

  Serial.println(location +actuator +value);
}

int postRequest(String URL, String data, String contentType){

  int httpCode; //HTTP response code

  HTTPClient http; //Connection instance

  Serial.print("[HTTP] begin...\n");
  if (http.begin(URL)) {  //Successful connection

    http.addHeader("Content-Type", contentType); //Header to change the content type if JSON is POSTED

    Serial.print("[HTTP] POST...\n");
    Serial.println(data);
    httpCode = http.POST(data); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    return httpCode;
  } else {
    Serial.printf("[HTTP] Unable to connect\n");
    return 404;
  }
}
