#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>

//WiFiMulti instance, on setup you can enter many different APs' credentials
ESP8266WiFiMulti WiFiMulti;

// Status of WiFi connection
bool connected = false;

//Store GET Request's response payload
String payload;

// FUNCTIONS
int postRequest(String URL, String data, String contentType = "text/plain");
void sleepSeconds(int seconds);

void setup() {

  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  //WifiMulti let's you add many wifi APs credentials and connects to the one available
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("SSID", "PASSWORD");
}

void loop() {
  int connectionFails = 0;
  // if the WiFi is connected
  if (wifiMulti.run() == WL_CONNECTED) {
    // And was not connected previously
    if (!connected) {

      connected = true; // update its status
      Serial.println("Connection established!"); // notify the user
      Serial.print("IP address:\t"); // print the new IP address
      Serial.println(WiFi.localIP());

      //---- Code Executed on First Connection ----
      String url = "http://192.168.1.3:3000/postData";
      String json = "{\"location\":\"livingroom\",\"actuator\":\"lights\",\"range\":{\"min\":\"off\",\"max\":\"on\"}}";

      // Retry until the POST request is successful and the microcontroller known to the network
      int code = -1;
      while(code < 0){
        code = postRequest(url, json);
        delay(500);
      }
    }
    //---- LOOP while connected ----

    //---- END of loop ----

  // WiFi not connected
  }else{
    connected = false; // update its status
    Serial.println("WiFi Disconnected!");
    delay(1000);
    connectionFails++;
    if (connectionFails > 15)
      sleepMinutes(10);
  }

} // end of loop() function



int postRequest(String URL, String data, String contentType){

  int httpCode; //HTTP response code

  HTTPClient http; //Connection instance

  Serial.print("[HTTP] begin...\n");
  if (http.begin(URL)) {  //Successful connection

    http.addHeader("Content-Type", contentType); //Header to change the content type if JSON is POSTED

    Serial.print("[HTTP] POST...\n");
    Serial.println(data);
    httpCode = http.POST(data); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    return httpCode;
  } else {
    Serial.printf("[HTTP] Unable to connect\n");
    return 404;
  }
}

//Maximum time of sleep cannot exceed 71 minutes because of integer overflow
void sleepMinutes(int minutes){
  ESP.deepSleep(minutes * 60000000);
}
