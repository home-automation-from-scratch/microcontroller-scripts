#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>
#include "DHTesp.h"

//WiFiMulti instance, on setup you can enter many different APs' credentials
ESP8266WiFiMulti WiFiMulti;

// Status of WiFi connection
bool connected = false;

// Flag set true if data is posted
bool posted = false;

//Store GET Request's response payload
String payload;

//DHT Device
DHTesp dht;

// FUNCTIONS
int postRequest(String URL, String data, String contentType = "application/json");
int postData();
void sleepMinutes(int minutes);

// Count the failed attempts to connect to the network
uint8_t maxConnectionFails = 15;
uint8_t connectionFails = 0;

void setup() {

  connectionFails = 0; // Reset counter on recovery from sleep

  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  // Initiallize device, define PIN and device Type
  dht.setup(D4, DHTesp::DHT11);

  //WifiMulti let's you add many wifi APs credentials and connects to the one available
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("COSMOTE-A455A0", "2310904434");
}

void loop() {
  if (posted){
    delay(120000);
    posted = false;
  }

  // if the WiFi is connected
  if (WiFiMulti.run() == WL_CONNECTED) {
    // And was not connected previously
    if (!connected) {

      connected = true; // update its status
      Serial.println("Connection established!"); // notify the user
      Serial.print("IP address:\t"); // print the new IP address
      Serial.println(WiFi.localIP());

      //---- Code Executed on First Connection ----



    // Sleep for 30 minutes after posting data
    //sleepMinutes(30);
    //delay(600000);

    }
    //---- LOOP while connected ----

    postData();
    posted = true;

    //---- END of loop ----

  // WiFi not connected
  }else{
    connected = false; // update its status
    Serial.println("WiFi Disconnected!");
    delay(1000);
    connectionFails++;
    if (connectionFails > maxConnectionFails)
      delay(60000);
      //sleepMinutes(1);
  }

} // end of loop() function

// Posts specific to device Data
int postData(){
  // Read brightness, temperature and humidity values
  int brightness = map(analogRead(A0), 0, 1024, 0, 100);
  float humidity = dht.getHumidity();
  float temperature = dht.getTemperature();
  String url = "http://192.168.1.2:3000/postData";
  String jsonTemperature = "{\"location\":\"livingroom\",\"sensor\":\"temperature\",\"value\":" + String(temperature) + "}";
  String jsonHumidity = "{\"location\":\"livingroom\",\"sensor\":\"humidity\",\"value\":" + String(humidity) + "}";
  String jsonBrightness = "{\"location\":\"livingroom\",\"sensor\":\"brightness\",\"value\":" + String(brightness) + "}";

  // Retry until the POST request is successful or maximum retries are due
  int code = -1;
  uint8_t retries = 0;
  uint8_t maxRetries = 5;
  while(code < 0){
    retries++;
    code = postRequest(url, jsonTemperature);
    delay(500);
  }
  code = -1;
  retries = 0;
  while(code < 0){
    retries++;
    code = postRequest(url, jsonHumidity);
    delay(500);
  }
  code = -1;
  retries = 0;
  while(code < 0){
    retries++;
    code = postRequest(url, jsonBrightness);
    delay(500);
  }
  return code;
}

int postRequest(String URL, String data, String contentType){

  int httpCode; //HTTP response code

  HTTPClient http; //Connection instance

  Serial.print("[HTTP] begin...\n");
  if (http.begin(URL)) {  //Successful connection

    http.addHeader("Content-Type", contentType); //Header to change the content type if JSON is POSTED

    Serial.print("[HTTP] POST...\n");
    Serial.println(data);
    httpCode = http.POST(data); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    return httpCode;
  } else {
    Serial.printf("[HTTP] Unable to connect\n");
    return 404;
  }
}

//Maximum time of sleep cannot exceed 71 minutes because of integer overflow
void sleepMinutes(int minutes){
  ESP.deepSleep(minutes * 60000000);
}
