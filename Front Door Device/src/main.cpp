#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <MFRC522.h>
#define RST_PIN	5 // RST-PIN for RC522 - RFID - SPI - Modul GPIO5
#define SS_PIN	4 // SDA-PIN for RC522 - RFID - SPI - Modul GPIO4

// ---- CONFIG ----
String httpEndpointURL = "http://192.168.1.4:3000";

// --- RFID ---
MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance
byte uid[4]; // Byte array to store the UID
void postuid(byte *buffer, byte bufferSize, String URL);
void checkForCard();
// --- RFID ---


// --- Web Server ---
String data = "";
bool dataReceived = false;

// Object for storin multiple APs' credentials
ESP8266WiFiMulti wifiMulti;

// Status of WiFi connection
bool connected = false;

// Object for server, more info at
// https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WebServer
ESP8266WebServer server(80);

void handleRequest();
void handleBody(String data);
void handleCommand(String location, String actuator, String value);
int postRequest(String URL, String data, String contentType = "application/json");
void checkForCard();
void postuid(byte *buffer, byte bufferSize, String URL);
// --- Web Server ---


void setup() {

  pinMode(16, OUTPUT);

  // Initiallize communication with RFID Reader
  SPI.begin(); // Initialize SPI bus
  mfrc522.PCD_Init();  // Initialize MFRC522

  // Begin Serial Communication
  Serial.begin(115200);

  // WiFi set as station aka client
  WiFi.mode(WIFI_STA);

  // Add your known networks
  wifiMulti.addAP("ssid_from_AP_2", "your_password_for_AP_2");
  wifiMulti.addAP("ssid_from_AP_2", "your_password_for_AP_2");
  wifiMulti.addAP("ssid_from_AP_3", "your_password_for_AP_3");

  // Attach routes to functions,
  // you know, like evey other freaking backend framework
  server.on("/actuate", handleRequest);

  // Start the server, obviously
  server.begin();

}

void loop() {
  // if the WiFi is connected
  if (wifiMulti.run() == WL_CONNECTED) {
    // And was not connected previously
    if (!connected) {

      connected = true; // update its status
      Serial.println("Connection established!"); // notify the user
      Serial.print("IP address:\t"); // print the new IP address
      Serial.println(WiFi.localIP());

      //---- Code Executed on First Connection ----
      String url = httpEndpointURL + "/actuatorDeployed";
      String json = "{\"location\":\"entrance\",\"actuator\":\"door\",\"range\":{\"min\":\"close\",\"max\":\"open\"}}";

      // Retry until the POST request is successful and the microcontroller known to the network
      int code = -1;
      while(code < 0){
        code = postRequest(url, json);
        delay(500);
      }
    }
    //---- LOOP while connected ----

    // Handling of incoming requests
    server.handleClient();

    // Check for new Card in range
    // The UID of the card will be POSTed
    checkForCard();


    //---- END of loop ----

  // WiFi not connected
  }else{
    connected = false; // update its status
    if (!connected)
      Serial.println("WiFi Disconnected!");
    delay(1000);
  }

} // end of loop() function

// Handle the incoming request on /actuate route
void handleRequest(){

  // clear data String to populate with the new request body
  data = "";

  // Check if body received
  if (server.hasArg("plain")== false){
    server.send(200, "text/plain", "Body not received");
    return;
  }

  //
  Serial.println("YAY! New Request!");
  dataReceived = true;
  String message = "Body received:\n";
         data += server.arg("plain"); // Request Body
         message += data;
         message += "\n";
  server.send(200, "text/plain", "OK"); // Response

  handleBody(data);
}

// Handles the request body
// Parse the Json input and
// Actuate the actuators
void handleBody(String data){

  // Allocate a temporary JsonDocument
  StaticJsonDocument<1024> doc;

  // Deserialize the JSON string
  DeserializationError error = deserializeJson(doc, data);

  // Test if parsing succeeds.
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  String location = doc["location"];
  String actuator = doc["actuator"];
  String value = doc["value"];

  handleCommand(location, actuator, value);
}

void handleCommand(String location, String actuator, String value){
  Serial.println(location +actuator +value);
  if(location == "entrance" && actuator == "door"){
    if (value == "open"){
      digitalWrite(16, HIGH);
      Serial.println("Door Opened");
    }
    else if (value == "close"){
      Serial.println("Door Closed");
      digitalWrite(16, LOW);
    }
  }
}

int postRequest(String URL, String data, String contentType){

  int httpCode; //HTTP response code

  HTTPClient http; //Connection instance

  Serial.print("[HTTP] begin...\n");
  if (http.begin(URL)) {  //Successful connection

    http.addHeader("Content-Type", contentType); //Header to change the content type if JSON is POSTED

    Serial.print("[HTTP] POST...\n");
    Serial.println(data);
    httpCode = http.POST(data); //start connection and send HTTP header

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);

    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
    return httpCode;
  } else {
    Serial.printf("[HTTP] Unable to connect\n");
    return 404;
  }
}

void checkForCard(){
  // Look for new cards
  if (mfrc522.PICC_IsNewCardPresent()) {
    delay(50);
    // Verify if the NUID has been readed
    if (mfrc522.PICC_ReadCardSerial()) {
      delay(50);
      String url = httpEndpointURL + "/postUrgent";
      postuid(mfrc522.uid.uidByte, mfrc522.uid.size, url);
    }
  }
}

// Store the uid to the uid[] variable
void postuid(byte *buffer, byte bufferSize, String URL){
  // Store byte by byte
  for (byte i = 0; i < bufferSize; i++) {
    uid[i] = buffer[i];
  }
  // Build String to be POSTed
  String uids = "";
  for (byte i = 0; i < 4; i++){
    uids += uid[i];
    if (i<3) uids += "-";
  }
  String json = "{\"location\":\"entrance\",\"sensor\":\"rfid\",\"value\":\"" + uids + "\"}";
  postRequest(URL, json);
}
