#include "MFRC522.h"
#define RST_PIN	5 // RST-PIN for RC522 - RFID - SPI - Modul GPIO5
#define SS_PIN	4 // SDA-PIN for RC522 - RFID - SPI - Modul GPIO4

// Create MFRC522 instance
MFRC522 mfrc522(SS_PIN, RST_PIN);

// Byte array to store the UID
byte uid[4];

// Function for storing the UID to the uid[] variable
void saveuid(byte *buffer, byte bufferSize);

void checkForCard();

 void setup() {
   Serial.begin(115200);  // Initialize serial communications
   SPI.begin(); // Init SPI bus
   mfrc522.PCD_Init();  // Init MFRC522
 }

 void loop() {
   checkForCard();
 }

void checkForCard(){
  // Look for new cards
  if (mfrc522.PICC_IsNewCardPresent()) {
    delay(50);
    // Verify if the NUID has been readed
    if (mfrc522.PICC_ReadCardSerial()) {
      delay(50);
      saveuid(mfrc522.uid.uidByte, mfrc522.uid.size);
    }
  }
}

// Store the uid to the uid[] variable
void saveuid(byte *buffer, byte bufferSize) {
  Serial.print(F("Card UID:"));
  // Store byte by byte
  for (byte i = 0; i < bufferSize; i++) {
    uid[i] = buffer[i];
  }
  // Print on Serial
  for (byte i = 0; i < 4; i++){
    Serial.print(uid[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
}
